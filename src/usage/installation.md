# Installation

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

FINAM can be installed using `pip`, from a local clone of the Git repository. See the [`pip` website](https://pypi.org/project/pip/) for how to get `pip`.

1. Install FINAM from the Git repository:

```shell
$ pip install git+https://git.ufz.de/FINAM/finam.git
```

2. Test it

```shell
$ python
```

```python
>>> import finam
>>> print(finam.__version__)
```

Congratulations! You can now start using FINAM.
