# Using FINAM

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

The following chapters target users that want to couple models that are already prepared for FINAM.

Chapter [Installation](./installation.md) covers how to install the FINAM Python package.

Chapter [Model coupling scripts](coupling_scripts.md) covers how to write Python scripts that compose and run model coupling setups.

Chapter [Known components and adapters](usage/components_adapters.md) provides a list of known FINAM components and adapters for the use in coupling scripts.
