# Components and adapters

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

This chapter lists known components and adapters for use in FINAM compositions.

## Included in FINAM core package

## Provided by FINAM developers

## Known 3rd party
