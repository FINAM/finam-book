# About this book

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

This book has multiple purposes:

* [FINAM principles](./principles/_intro.md): Explain the basic principles of FINAM to the interested audience
* [Using FINAM](./usage/_intro.md): Teach end users how to set up and run FINAM compositions
* [Developing for FINAM](./development/_intro.md): Teach developers how to implement FINAM modules, and how to wrap existing models for coupling

All except the chapters on principles require some Python programming skills.
We do not teach Python in this book, so different levels of programming knowledge
are assumed for certain chapters. Read on for details...

## Requirements

The first chapters under [FINAM principles](./principles/_intro.md) are crucial for understanding how FINAM works.
They contain only textual and visual descriptions. They contain no code and require no programming knowledge.
Read this first!

The chapters under [Using FINAM](./usage/_intro.md) are dedicated to users
that want to set up FINAM compositions using existing modules.
Compositions are created through Python scripts.
These chapters require some basic Python knowledge for writing simple scripts.

The chapters under [Developing for FINAM](./development/_intro.md) are dedicated to developers
that want to build modules for FINAM, or wrap existing models or libraries for the use in FINAM compositions.
To write modules following these chapters, intermediate knowledge of Python is required.
Particularly, developers need some basic understanding of object-oriented programming in Python.
