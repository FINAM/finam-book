# FINAM principles

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

This chapter describes:

* What [Components and Adapters](./components_adapters.md) are
* How [Coupling and Scheduling](./coupling_scheduling.md) works
* How [Data flow](./data_flow.md) is managed
* How FINAM treats [Metadata and time](metadata.md)
