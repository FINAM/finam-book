# FINAM -- Introduction

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

FINAM is an open-source component-based model coupling framework for environmental models.
It aims at enabling bi-directional online couplings of models for different compartments like geo-, hydro-, pedo- and biosphere.

<a href="https://finam.pages.ufz.de" title="FINAM homepage" target="_blank">
  <img width="300" src="./images/logo_large.svg" />
</a>

The framework is built in Python, with well-defined interfaces for data exchange.
This approach allows for coupling of models irrespective of their internal structure, architecture or programming language.

### Chapters

* [About this book](./about.md) -- The purpose of this book, and how to read it.
* [FINAM principles](./principles/_intro.md) -- Explains basic principles of the FINAM framework that are of interest for users as well as developers.
* [Using FINAM](./usage/_intro.md) -- Guide for users that aim at coupling existing models.
* [Developing for FINAM](./development/_intro.md) -- Guide for developers on how to prepare models for FINAM, and how to implement adapters.

### External resources

* FINAM [homepage](https://finam.pages.ufz.de)
* FINAM [source code](https://git.ufz.de/FINAM/finam) and [API docs](https://finam.pages.ufz.de/finam/)
* FINAM [GitLab group](https://git.ufz.de/FINAM), containing further related projects
* [Sources](https://git.ufz.de/FINAM/finam-book) of this book
