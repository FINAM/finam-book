# Python bindings

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

FINAM requires Python bindings for coupling components or models written in a language other than Python, like C++, Fortran or Rust.

Implementation of bindings is highly dependent on the language, as well as the approach.
Therefore, we provide a [repository with example projects](https://git.ufz.de/FINAM/pybind-examples) for different languages and approaches.

## Recommendations

[TODO]
