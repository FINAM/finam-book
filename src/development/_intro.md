# Developing for FINAM

----

**This book has moved to the [FINAM documentation](https://finam.pages.ufz.de/finam/). You are viewing an outdated version!**

----

The following chapters target developers that want to prepare their existing models for use in FINAM, or that want to write FINAM-compatible models from scratch.

Besides preparation of models, implementation of adapters that mediate data between models is also covered.
