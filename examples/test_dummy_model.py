import unittest
from datetime import datetime, timedelta

import finam as fm


class DummyModel(fm.TimeComponent):
    def __init__(self, start, step):
        super().__init__()
        self._step = step
        self.time = start

    def _initialize(self):
        self.inputs.add(name="A", time=None, grid=fm.NoGrid())
        self.inputs.add(name="B", time=None, grid=fm.NoGrid())
        self.outputs.add(name="Sum", time=None, grid=fm.NoGrid())

        self.create_connector()

    def _connect(self):
        self.try_connect(time=self.time, push_data={"Sum": 0})

    def _validate(self):
        pass

    def _update(self):
        a = self.inputs["A"].pull_data(self.time)
        b = self.inputs["B"].pull_data(self.time)

        result = a + b

        # We need to unwrap the data here, as the push time will not equal the pull time.
        # This would result in conflicting timestamps in the internal checks
        result = fm.data.strip_data(result)

        self._time += self._step

        self.outputs["Sum"].push_data(result, self.time)

    def _finalize(self):
        pass


class TestDummy(unittest.TestCase):
    def test_dummy_model(self):
        model = DummyModel(start=datetime(2000, 1, 1), step=timedelta(days=7))
        generator = fm.modules.generators.CallbackGenerator(
            callbacks={
                "A": (lambda t: t.day, fm.Info(time=None, grid=fm.NoGrid())),
                "B": (lambda t: t.day * 2, fm.Info(time=None, grid=fm.NoGrid())),
            },
            start=datetime(2000, 1, 1),
            step=timedelta(days=7),
        )
        consumer = fm.modules.debug.DebugConsumer(
            inputs={"Sum": fm.Info(time=None, grid=fm.NoGrid())},
            start=datetime(2000, 1, 1),
            step=timedelta(days=7),
        )
        composition = fm.Composition([model, generator, consumer], log_level="DEBUG")
        composition.initialize()

        generator.outputs["A"] >> model.inputs["A"]
        generator.outputs["B"] >> model.inputs["B"]

        model.outputs["Sum"] >> consumer.inputs["Sum"]

        composition.connect()

        self.assertEqual(consumer.data, {"Sum": 0})

        composition.run(t_max=datetime(2000, 12, 31))
