import random
from datetime import datetime, timedelta

import finam as fm
from finam.adapters import base, time
from finam.modules import generators, visual

if __name__ == "__main__":
    # Instantiate components, e.g. models

    # Here, we use a simple component that outputs a random number each step
    generator = generators.CallbackGenerator(
        {"Value": (lambda _t: random.uniform(0, 1), fm.Info(grid=fm.NoGrid()))},
        start=datetime(2000, 1, 1),
        step=timedelta(days=10),
    )

    # A live plotting component
    plot = visual.time_series.TimeSeriesView(
        inputs=["Value"],
        start=datetime(2000, 1, 1),
        step=timedelta(days=1),
        intervals=[1],
    )

    # Create two adapters for...
    # temporal interpolation
    time_interpolation_adapter = time.LinearTime()
    # data transformation
    square_adapter = base.Callback(lambda x, _time: x * x)

    # Create a `Composition` containing all components
    composition = fm.Composition([generator, plot])

    # Initialize the `Composition`
    composition.initialize()

    # Couple inputs to outputs, via multiple adapters
    (
        generator.outputs["Value"]
        >> time_interpolation_adapter
        >> square_adapter
        >> plot.inputs["Value"]
    )

    # Run the composition until January 2000
    composition.run(datetime(2001, 1, 1))
