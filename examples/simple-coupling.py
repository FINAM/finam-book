import random
from datetime import datetime, timedelta

import finam as fm
from finam.modules.generators import CallbackGenerator
from finam.modules.visual.time_series import TimeSeriesView

if __name__ == "__main__":
    # Instantiate components, e.g. models

    # Here, we use a simple component that outputs a random number each step
    generator = CallbackGenerator(
        {"Value": (lambda _t: random.uniform(0, 1), fm.Info(grid=fm.NoGrid()))},
        start=datetime(2000, 1, 1),
        step=timedelta(days=1),
    )

    # A live plotting component
    plot = TimeSeriesView(
        inputs=["Value"],
        start=datetime(2000, 1, 1),
        step=timedelta(days=1),
        intervals=[1],
    )

    # Create a `Composition` containing all components
    composition = fm.Composition([generator, plot])

    # Initialize the `Composition`
    composition.initialize()

    # Couple inputs to outputs
    generator.outputs["Value"] >> plot.inputs["Value"]

    # Run the composition until January 2001
    composition.run(datetime(2001, 1, 1))
